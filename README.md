# cf_dns_update

Script to update DNS names on [Cloudflare].  I use this to setup my own DDNS service, using my own domain, since [Cloudflare] performs quick host updates.


## Installing

It is advisable to have a virtual environment set to run this script, just to avoid mixing it's requirements with global system's modules.



## Configuration

Two files are needed and samples are provided: `mysecrets.py` and `check_ip.ini`.

```bash
cp mysecrets.py.sample mysecrets.py
cp check_ip.ini.sample check_ip.ini
```

Now edit both files and fill with your own information:

### check_ip.ini

```ini
[default]
hostname = local_hostname
domain = local_domain_name
dns_host = host_on_cloudflare
trigram = TST  # host_identification_for
admin_email = test@test.com
slack_web_hook = https://hooks.slack.com/services/...
slack_admin_group = admin

```

### mysecrets.py

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class Secrets:
    CLOUDFLARE_TOKEN = 'CloUDFlareT0k3n'
    ZONE_NAME = 'test.com'
    CF_EMAIL = 'test@test.com'

```


Project Icon: <div>Icons made by <a href="https://www.flaticon.com/authors/eucalyp" title="Eucalyp">Eucalyp</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>


[Cloudflare]: https://cloudflare.com
