#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to update client's host on Cloudflare
"""
import os
import json
import yaml
import socket
from requests import get as get_url, post as post_url, Session, adapters
import urllib3
from urllib3.poolmanager import PoolManager
import ipaddress
import CloudFlare.exceptions
import CloudFlare
from sys import exit
from configparser import ConfigParser
from loguru import logger
import urllib3.util.connection as urllib3_cn

urllib3.disable_warnings()

try:
    from mysecrets import Secrets

    s = Secrets()
    CLOUDFLARE_TOKEN = s.CLOUDFLARE_TOKEN

except ModuleNotFoundError:
    CLOUDFLARE_TOKEN = os.getenv('CLOUDFLARE_TOKEN', None)

if not CLOUDFLARE_TOKEN:
    print('The CLOUDFLARE_TOKEN is not defined')
    exit(1)

DNS_UPDATE_URL = 'https://fboaventura.dev/ip'
BASE_DIR = os.path.dirname((os.path.abspath(__file__)))
LOG_DIR = os.path.join(BASE_DIR, 'log')
HTTP_REQUEST_HEADERS = {
    'User-Agent': 'curl/7.54.0',
    'Host': DNS_UPDATE_URL.split('/')[2],
}

logger.add(f'{LOG_DIR}/dns_update.log', rotation='10 MB', compression='gz')


class InterfaceAdapter(adapters.HTTPAdapter):
    # https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
    # https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-of-eth0-in-python
    """
    A TransportAdapter that reuses connections for the same host.

    This is a copy of the default adapter, but with the socket_options
    parameter added to the PoolManager constructor.

    Args:
        interface: The interface to bind to.

    Attributes:
        interface: The interface to bind to.

    Methods:
        _socket_options: Get the socket options to bind to an interface.
        init_poolmanager: Initialize the PoolManager.

    Raises:
        None

    Examples:
        >>> adapter = InterfaceAdapter(interface='eth0')
        >>> adapter._socket_options()
        [(41, 25, b'eth0')]

        >>> adapter.init_poolmanager(10, 10)
        >>> adapter.poolmanager
        <urllib3.poolmanager.PoolManager object at 0x7f8b0c0b4e10>
    """

    def __init__(self, **kwargs):
        """
        Initialize the InterfaceAdapter class

        Args:
            **kwargs: Arbitrary keyword arguments.
                interface: The interface to bind to.

        Returns:
            None

        Raises:
            None
        """
        self.interface = kwargs.pop('interface', None)
        super().__init__(**kwargs)

    def _socket_options(self):
        """
        Get the socket options to bind to an interface.

        Args:
            None

        Returns:
            A list of socket options.
        """
        if self.interface is None:
            return []
        return [(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, self.interface.encode())]

    def init_poolmanager(self, connections, maxsize, block=False, **pool_kwargs):
        """
        Initialize the PoolManager.

        Args:
            connections: The number of connections.
            maxsize: The maximum size of the pool.
            block: Boolean to block.
            **pool_kwargs: Arbitrary keyword arguments.

        Returns:
            None
        """
        self.poolmanager = PoolManager(
            num_pools=connections,
            maxsize=maxsize,
            block=block,
            socket_options=self._socket_options(),
            **pool_kwargs
        )


class Config:
    """
    Class to read configuration files

    Args:
        config_file: Configuration file to read

    Attributes:
        config: Configuration file content
        _yaml: Boolean to identify if the configuration file is a YAML file

    Methods:
        get: Get a value from the configuration file

    Raises:
        None

    Examples:
        >>> config = Config('config.yml')
        >>> config.get('cloudflare', 'token')
        '1234567890abcdef'

        >>> config = Config('config.ini')
        >>> config.get('cloudflare', 'token')
        '1234567890abcdef'

    """
    def __init__(self, config_file):
        """
        Initialize the Config class

        Args:
            config_file: Configuration file to read

        Returns:
            None
        """
        if config_file.endswith('.yml') or config_file.endswith('.yaml'):
            logger.info('Reading YAML configuration file')
            self.cfg = yaml.safe_load(open(config_file))
            self.is_yaml = True
        else:
            logger.info('Reading INI configuration file')
            self.cfg = ConfigParser()
            self.cfg.read(config_file)
            self.is_yaml = False

    def get(self, section, key):
        """
        Get a value from the configuration file

        Args:
            section: Section of the configuration file
            key: Key of the configuration file

        Returns:
            Value of the configuration file
        """
        if self.is_yaml:
            return self.cfg[section][key]
        else:
            return self.cfg.get(section, key)


def allowed_gai_family():
    """
    Function to force connection through IPv4

    Args:
        None

    Returns:
        socket.AF_INET

    Raises:
        None

    Examples:
        >>> allowed_gai_family()
        2
    """
    family = socket.AF_INET

    return family


def update_slack(webhook: str, message: str):
    """
    Send a message to a Slack channel

    Args:
        webhook: Slack webhook URL
        message: Message to send to Slack

    Returns:
        None

    Raises:
        None

    Examples:
        >>> update_slack('https://hooks.slack.com/services/1234567890/1234567890/1234567890abcdef', 'Hello World!')

    """
    payload = {"text": f"{message}"}

    try:
        data = json.dumps(payload)
        rsp = post_url(
            webhook,
            data=data.encode('ascii'),
            headers={'Content-Type': 'application/json'}
        )
        if rsp:
            logger.info('Message posted to slack successfully!')

    except Exception as em:
        logger.error("Oops! Something nasty just happened: {}".format(str(em)))


def my_ip_address(update_url: str = DNS_UPDATE_URL, interface: str = None):
    """
    Identify the public IP of the running instance

    Args:
        update_url: URL to get the public IP address
        interface: Interface to bind to

    Returns:
        ip_address: Public IP address
        address_type: IP address type (A or AAAA)

    Raises:
        None

    Examples:
        >>> my_ip_address()
        ('8.8.8.8', 'A')

        >>> my_ip_address('https://ipv6.fboaventura.dev/ip')
        ('2001:db8::1', 'AAAA')
    """
    urllib3_cn.allowed_gai_family = allowed_gai_family
    ip_address = None
    try:
        if interface:
            session = Session()
            for scheme in ('http://', 'https://'):
                session.mount(scheme, InterfaceAdapter(interface=interface))
            ip_address = session.get(update_url, headers=HTTP_REQUEST_HEADERS, verify=False).text.strip()
        else:
            ip_address = get_url(update_url, headers=HTTP_REQUEST_HEADERS, verify=False).text.strip()

        if ip_address == '':
            logger.error(f'Failed to get address from {update_url}')
            exit(1)
    except Exception as err:
        logger.error(f'Failed to get address from {update_url} ({err}')
        exit(1)

    address_type = 'AAAA' if ipaddress.ip_address(ip_address).version == 6 else 'A'

    return ip_address, address_type


def resolve_hostname(hostname: str):
    """
    Resolve a hostname to an IP address

    Args:
        hostname: Hostname to resolve

    Returns:
        ip_address: IP address of the hostname

    Raises:
        None

    Examples:
        >>> resolve_hostname('fboaventura.dev')
        '8.8.8.8'
    """
    try:
        return socket.gethostbyname(hostname)
    except socket.gaierror:
        return '127.0.0.1'


def get_cf_conn(token: str) -> CloudFlare.CloudFlare:
    """
    Connect to CloudFlare API

    Args:
        token: CloudFlare API token

    Returns:
        CloudFlare connection

    Raises:
        None

    Examples:
        >>> get_cf_conn('1234567890abcdef')
        <CloudFlare.CloudFlare object at 0x7f8b0c0b4e10>
    """
    try:
        return CloudFlare.CloudFlare(token=token)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        logger.error(f'Failed to connect to CloudFlare API ({e})')
        exit(1)


def get_zone(conn: CloudFlare.CloudFlare, zone_name: str):
    """
    Get the zone record from CloudFlare

    Args:
        conn: CloudFlare connection
        zone_name: Zone name to get the record

    Returns:
        CloudFlare zone record

    Raises:
        None

    Examples:
        >>> get_zone(conn, 'example.com')
        <CloudFlare.models.zones.Zone object at 0x7f8b0c0b4e10>

    """
    try:
        zone_record = conn.zones.get(params={'name': zone_name})[0]
    except CloudFlare.exceptions.CloudFlareAPIError as err:
        logger.error(f'/zones/get {zone_name} - Failed to connect: {err}')
        exit(1)
    except IndexError:
        logger.error(f'Zone {zone_name} was not found on CloudFlare')
        exit(1)

    return zone_record


def check_if_host_exists(conn, cf_zone_id, cf_hostname) -> dict:
    """
    Check if the hostname exists on CloudFlare

    Args:
        conn: CloudFlare connection
        cf_zone_id: CloudFlare zone ID
        cf_hostname: Hostname to check

    Returns:
        Dictionary with the status and the record

    Raises:
        None

    Examples:
        >>> check_if_host_exists(conn, '1234567890abcdef', 'example.com')
        {'status': 'active', 'record': {
        'id': '1234567890abcdef',
        'content': 'example.com',
        'type': 'A',
        'proxiable': True,
        'proxied': False,
        'ttl': 1,
        'name': 'example.com',
        'zone_id': '1234567890abcdef',
        'zone_name': 'example.com'}
        }

    """
    try:
        params = {'name': cf_hostname, 'match': 'all'}
        dns_record = conn.zones.dns_records.get(cf_zone_id, params=params)[0]
        if len(dns_record) > 0:
            return {
                'status': 'active',
                'record': {
                    'id': dns_record['id'],
                    'content': dns_record['content'],
                    'type': dns_record['type'],
                    'proxiable': dns_record['proxiable'],
                    'proxied': dns_record['proxied'],
                    'ttl': 1,
                    'name': dns_record['name'],
                    'zone_id': dns_record['zone_id'],
                    'zone_name': dns_record['zone_name'],
                    'comment': dns_record['comment'] if 'comment' in dns_record else 'Updated by CloudFlare DDNS'
                }}
        else:
            return {'status': 'inactive'}
    except CloudFlare.exceptions.CloudFlareAPIError as err:
        logger.error(f'/zones/dns_records {cf_hostname} - Failed to get hostname: {err}')
        exit(1)
    except IndexError:
        logger.info(f'Record {cf_hostname} was not found!')
        return {'status': 'not_found'}


def record_update(conn, zone, dns_name, ip_address, address_type):
    """
    Update the DNS record on CloudFlare

    Args:
        conn: CloudFlare connection
        zone: CloudFlare zone record
        dns_name: DNS name to update
        ip_address: IP address to update
        address_type: IP address type (A or AAAA)

    Returns:
        None

    Raises:
        Exception: If the hostname could not be updated

    Examples:
        >>> record_update(conn, zone, 'example.com', '8.8.8.8', 'A')
        None
    """
    _is_record = check_if_host_exists(conn, zone['id'], dns_name)
    logger.info(f'record: {_is_record}')
    is_updated = False
    dns_record = old_ip_address = None

    if _is_record['status'] == 'active':
        record = _is_record['record']
        old_ip_address = record['content']
        old_ip_type = record['type']

        if (address_type and old_ip_type) not in ['A', 'AAAA']:
            logger.error(f'IGNORED: {dns_name} {old_ip_address}; Record type not allowed')
            return {"status": "ignored", "hostname": dns_name, "old_ip": old_ip_address, "new_ip": ip_address, "reason": "Record type not allowed"}

        if ip_address == old_ip_address:
            logger.info(f'UNCHANGED: {dns_name} {ip_address}')
            is_updated = True
            return {"status": "unchanged", "hostname": dns_name, "old_ip": old_ip_address, "new_ip": ip_address}

        if not is_updated:
            record_data = {
                "name": dns_name,
                "type": address_type,
                "content": ip_address,
                "ttl": 1,
                "proxied": record['proxied'],
                "comment": "Updated by CloudFlare DDNS"
            }
            try:
                dns_record = conn.zones.dns_records.put(zone['id'], record['id'], data=record_data)
            except CloudFlare.exceptions.CloudFlareAPIError as err:
                logger.error(f'/zones/dns_records {dns_name} - Failed to update hostname: {err}')
            except IndexError:
                logger.info(f'Record {dns_name} was not found!')

            logger.info(f'dns_record: {dns_record}')
            logger.info(f'UPDATED: {dns_name} {old_ip_address} -> {ip_address}')

            return {"status": "updated", "hostname": dns_name, "old_ip": old_ip_address, "new_ip": ip_address}

    elif _is_record['status'] == 'not_found':
        record_data = {"name": f"{dns_name}", "type": address_type, "content": ip_address, "proxied": False, "ttl": 1}

        try:
            dns_record = conn.zones.dns_records.post(zone['id'], data=json.dumps(record_data))
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            logger.error(f'/zones/dns_records {dns_name} - Failed to create hostname: {e}')

        logger.info(f'CREATED: {dns_name} {ip_address}')
        return {"status": "created", "new_ip": ip_address}


if __name__ == '__main__':
    logger.info('Starting the DNS update process')
    logger.info('Checking config file')

    if os.path.exists(os.path.join(BASE_DIR, 'check_ip.yml')):
        cfg_file = os.path.join(BASE_DIR, 'check_ip.yml')
        logger.info(f'Found {cfg_file}')
    elif os.path.exists(os.path.join(BASE_DIR, 'check_ip.yaml')):
        cfg_file = os.path.join(BASE_DIR, 'check_ip.yaml')
        logger.info(f'Found {cfg_file}')
    elif os.path.exists(os.path.join(BASE_DIR, 'check_ip.ini')):
        cfg_file = os.path.join(BASE_DIR, 'check_ip.ini')
        logger.info(f'Found {cfg_file}')
    else:
        logger.error('Config file not found!')
        exit(1)

    config = Config(cfg_file)
    logger.info('Connecting to CloudFlare')
    cf = get_cf_conn(CLOUDFLARE_TOKEN)

    slack_web_hook = config.get('default', 'slack_web_hook')
    admin_group = config.get('default', 'slack_admin_group')

    logger.info('Getting the hostname(s) to update')
    hostname = config.get('default', 'dns_host') if not config.is_yaml else config.get('default', 'hosts')
    logger.info(f'Hostname(s): {hostname}')
    logger.info(f'type(hostname): {type(hostname)}')

    if isinstance(hostname, str):
        logger.info('Getting the Public IP Address')
        my_ip, addr_type = my_ip_address()
        domain_name = config.get('default', 'domain')

        # Check if the hostname is registered in the DNS and get the actual IP Address
        actual_ip = resolve_hostname(f'{hostname}.{domain_name}')

        zone = get_zone(cf, domain_name)
        update_result = record_update(cf, zone, f'{hostname}.{domain_name}', my_ip, addr_type)
        logger.info(f'Record Update: {update_result}')

        if update_result['status'] == 'unchanged':
            logger.info('IP Address is unchanged, exiting')
            exit(0)

        to_notify = f"!{admin_group}" if admin_group == "channel" or admin_group == "here" else f"@{admin_group}"

        update_slack(slack_web_hook, f"<{to_notify}> *{config.get('default', 'trigram')}*: "
                                     f"{hostname}.{domain_name} IP updated from _{actual_ip}_ to _*{my_ip}*_")

    elif isinstance(hostname, list):
        logger.info(f'Updating {len(hostname)} hostnames')
        for host in hostname:
            if 'domains' in host.keys():
                domains = host['domains']
            else:
                domains = config.get('default', 'domains')

            logger.info(f'Updating {host["dns_host"]} with domains {domains}')
            for domain in domains:
                logger.info(f'Getting the Public IP Address for {host["dns_host"]}.{domain}')
                iface = host.get('interface', None)
                my_ip, addr_type = my_ip_address(interface=iface)

                logger.info(f'The actual IP Address is {my_ip} and the type is {addr_type}')
                # Check if the hostname is registered in the DNS and get the actual IP Address
                actual_ip = resolve_hostname(f'{host["dns_host"]}.{domain}')

                zone = get_zone(cf, domain)
                update_result = record_update(cf, zone, f'{host["dns_host"]}.{domain}', my_ip, addr_type)
                logger.info(f'Record Update: {update_result}')

                if update_result['status'] == 'unchanged':
                    continue
                else:
                    to_notify = f"!{admin_group}" if admin_group == "channel" or admin_group == "here" else f"@{admin_group}"

                    update_slack(slack_web_hook, f"<{to_notify}> *{host['trigram']}*: "
                                                 f"{host['dns_host']}.{domain} IP updated from _{actual_ip}_ to _*{my_ip}*_")
